const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const app = express()

let contacts = [
    { name: 'Phattarapol', phoneNumber: 59160292 },
    { name: 'Thanonkorn', phoneNumber: 59160022 },
    { name: 'Satawat', phoneNumber: 57660096 },
    {name: 'Surasek',phoneNumber:59161198}
]

app.use(bodyParser.json())
app.use(cors())

/// TODO: Develop GET /contacts
app.get('/contacts', (req, res) => {
    res.json(contacts)
})

app.get("/contacts/:index", (req, res) => {
    let index = req.params.index
    json(contacts[index])
})

/// TODO: Develop POST /contacts
app.post('/contacts', (req, res) => {
    let newContact = req.body
    contacts.push(newContact)
    res.status(201).json(newContact)
})

app.listen(3000, () => {
    console.log('API Server started at port 3000')
})
